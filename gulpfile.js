// Основной модуль
import gulp from 'gulp';

// Импорт путей
import { path } from './gulp/config/path.js';

// Импорт общих планинов
import { plugins } from './gulp/config/plugins.js';

// Передаём значения в глобальную переменную
global.app = {
  path: path,
  gulp: gulp,
  plugins: plugins,
};

// Импорт задач
import { copy } from './gulp/tasks/copy.js';
import { reset } from './gulp/tasks/reset.js';
import { html } from './gulp/tasks/html.js';
import { server } from './gulp/tasks/server.js';
import { scssBuild } from './gulp/tasks/scss.js';
import { js } from './gulp/tasks/js.js';
import { imagesBuild } from './gulp/tasks/images.js';

// Наблюдатель за изминениями в файлах
function watcher() {
  gulp.watch(path.watch.files, copy);
  gulp.watch(path.watch.html, html);
  gulp.watch(path.watch.scss, scssBuild);
  gulp.watch(path.watch.js, js);
  gulp.watch(path.watch.images, imagesBuild);
}

const mainTasks = gulp.parallel(copy, html, scssBuild, js, imagesBuild);

//Построение сценариев выполнения задач
const dev = gulp.series(reset, mainTasks, gulp.parallel(watcher, server));
const build = gulp.series(reset, mainTasks);

// Экспорт сценариев
export { dev };
export { build };

// Выполнение сценария по умолчанию
gulp.task('default', dev);
